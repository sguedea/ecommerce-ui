import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // define the url that we're making a call to from our Spring Boot 
  private baseUrl = 'http://localhost:8080/api/products';

  // inject HttpClient
  constructor(private httpClient: HttpClient) { }


  // method to get our products
  getProductList(): Observable<Product[]> {
    return this.httpClient.get<GetResponse>(this.baseUrl).pipe(
      map(response => response._embedded.products)
    );
  }
  // this method returns an observable.
  // Map the JSON data from Spring Data REST to Product array

} // END OF CLASS

// adding a "supporting" interface to help us with the mapping
interface GetResponse {
  _embedded: {
    products: Product[];
  }
  /* this interface will unwrap the JSON from Spring Data REST 
  _embedded entry
  And it'll access the array products
  */ 
}
