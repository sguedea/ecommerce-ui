import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  // defined a property for our array of our interface
  products: Product[] = [];


  // dependency injection: injecting our service
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    // calling our listProducts() method
    this.listProducts();
  }

  // method that subscribes to our GET request method in our service
  listProducts() {
    this.productService.getProductList().subscribe(
      data => {
        this.products = data;
      }
    )
  }

}
